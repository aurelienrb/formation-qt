import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    id: element
    TableView {
        anchors.right: listView.left
        anchors.rightMargin: 20
        anchors.leftMargin: 50
        anchors.bottomMargin: 50
        anchors.topMargin: 50
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        model: modelMnemo
        TableViewColumn {
            title: qsTr("Name")
            width: 200
            role: "roleName"
        }
        TableViewColumn {
            title: qsTr("Type")
            width: 200
            role: "roleType"
        }
    }

    // composant qui affiche une seule ligne de la list
    Component {
        id: listViewDelegate
        Item {
            width: 180; height: 40
            Column {
                Text { text: '<b>Name:</b> ' + roleName }
                Text { text: '<b>ID:</b> ' + roleID }
            }
        }
    }

    ListView {
        id: listView
        x: 346
        width: 200
        anchors.topMargin: 50
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        anchors.right: parent.right
        anchors.rightMargin: 50
        model: modelMnemo
        delegate: listViewDelegate
    }

    ListModel {
        id: modelMnemo
        ListElement {
            roleName: "E_36_00"
            roleType: qsTr("Entrée")
            roleID: "2545255"
        }
        ListElement {
            roleName: "E_36_01"
            roleType: qsTr("Entrée")
            roleID: "294755"
        }
        ListElement {
            roleName: "E_38_00"
            roleType: qsTr("Sortie")
            roleID: "8475255"
        }
    }
}



/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1;anchors_x:28;anchors_y:76}D{i:4;anchors_height:100;anchors_y:126}
}
 ##^##*/
