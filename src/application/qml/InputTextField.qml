import QtQuick 2.0
import QtQuick.Controls 2.12

TextField {
    property int minInputLength: 1
    property bool inputValid: text.length >= minInputLength
    signal submitted(string inputText)

    background: Rectangle {
        border.color: "#808080"
        color: inputValid ? "lightgreen" : "red"
    }

    Keys.onReturnPressed: {
        if (inputValid) {
            submitted(text)
        }
    }

    Keys.onEnterPressed: {
        if (inputValid) {
            submitted(text)
        }
    }

}
