#pragma once

#include <QObject>
#include <QString>

class LoginModel : public QObject {
    Q_OBJECT
public:
    Q_PROPERTY(bool loggedIn READ isLoggedIn NOTIFY loggedInChanged)
    Q_PROPERTY(bool loginInProgress READ isLoginInProgress NOTIFY loginInProgressChanged)

signals:
    // property change notification
    void loggedInChanged(bool);
    void loginInProgressChanged(bool);
    // signals emitted from the QML
    void startLogin(QString name, QString password);
    void logout();

public:
    bool isLoggedIn() const {
        return m_loggedIn;
    }
    void setLoggedIn(bool status) {
        if (m_loggedIn != status) {
            m_loggedIn = status;
            emit loggedInChanged(m_loggedIn);
        }
    }

    bool isLoginInProgress() const {
        return m_loginInProgress;
    }
    void setLoginInProgress(bool status) {
        if (m_loginInProgress != status) {
            m_loginInProgress = status;
            emit loginInProgressChanged(m_loginInProgress);
        }
    }

private:
    bool m_loggedIn        = false;
    bool m_loginInProgress = false;
};
