@echo off
REM script pour Windows afin de générer un projet pour VC++ (si ce n'est pas déjà fait)
REM puis l'ouvrir avec les variables d'environnement Qt correctement positionnées

REM chemin vers la version de Qt à utiliser
set QTDIR=C:\Qt\5.13.0\msvc2017
if not exist %QTDIR% (echo invalid Qt bin path: "%QTDIR%" && GOTO:FAILURE)

REM nom du répertoire de build pour cmake
set BUILDDIR=%~dp0cmake-msvc

REM rendre Qt visible
call "%QTDIR%\bin\qtenv2.bat" || GOTO:FAILURE

REM rendre VC++ & cmake visibles
REM (on utilise la version de cmake installée avec VC++ via l'option "C++ CMake tools for Windows" dans l'installeur)
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars32.bat" || GOTO:FAILURE

REM générer les fichiers projets si nécessaire
if not exist %BUILDDIR%\BeepingsTestbed.sln (
    if exist %BUILDDIR% rmdir /s/q %BUILDDIR%
    mkdir %BUILDDIR% || GOTO:FAILURE
    compact /c /q %BUILDDIR% > nul
    cd /D %BUILDDIR%
    cmake -G "Visual Studio 16 2019" -A Win32 .. || GOTO:FAILURE
    cd ..
)

REM ouvrir la solution VC++ (avec le PATH correctement configuré pour Qt)
start %BUILDDIR%\BeepingsTestbed.sln

REM ok!
GOTO:EOF

:FAILURE
echo Failure!
pause
