# doctest étant utilisé pour compiler les tests unitaires, on ne la rend disponible que si
# l'option associée a été activée
if (BUILD_TESTING)
    add_subdirectory(doctest-2.3.4)
endif()
