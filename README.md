# Formation C++ / Qt / QML

Exemple de projet C++ / QML basé sur CMake.

Au delà du squelette pour une application QML, ce projet contient également les éléments suivants :

- [x] haut niveau de warning des compilateurs C++ avec obligation de les traiter
- [x] formattage automatisé du code via `clang-format` (reconnu par des IDE comme Qt Creator, VC++, CLion)
- [x] tests unitaires C++ au moyen de la bibliothèque [doctest](https://github.com/onqtam/doctest)

## Installation

Sous Linux, étant donné que l'on utilise QML, il est nécessaire d'installer OpenGL comme expliqué ici : https://doc.qt.io/qt-5/linux.html

```
sudo apt-get install libgl1-mesa-dev
```

## Ouverture du projet depuis Qt Creator

Simplement ouvrir le fichier CMakeLists.txt à la racine du projet.

Il est possible d'activer la compilation des tests unitaires en cochant l'option `BUILD_TESTING` dans l'onglet `Projects -> Build Settings`.

![qtcreator-build-settings](documentation/qtcreator-build-settings.png)

> Tant qu'à faire, ajuster les options de Build pour passer l'argument `-j 8` à make afin de lui indiquer de compiler jusqu'à 8 fichiers à la fois au lieu d'un seul !

## Compilation en ligne de commande avec exécution des tests unitaires

```
mkdir cmake-build && cd cmake-build
cmake -DBUILD_TESTING=ON ..
make -j 8
ctest -V
```
