@echo off

REM il faut que clang-format.exe soit disponible dans le PATH
where clang-format.exe || exit /b 1

REM chemin complet vers le répertoire "src" qui est à formatter
set SRC=%~dp0src

for /r "%SRC%" %%f in ("*.cpp" "*.h") do (
    echo - %%f
    clang-format.exe -i %%f || pause
)
